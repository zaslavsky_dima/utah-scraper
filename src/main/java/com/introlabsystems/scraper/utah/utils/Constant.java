package com.introlabsystems.scraper.utah.utils;

public final class Constant {

    public static final String BASE_URL =
            "https://digitalcommons.law.byu.edu/utah_briefs_collection/index.html";
    public static final String STATE_NAME = "utah";

    private Constant() {
        throw new IllegalStateException("Utility class");
    }
}
