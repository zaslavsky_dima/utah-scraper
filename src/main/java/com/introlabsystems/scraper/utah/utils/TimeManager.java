package com.introlabsystems.scraper.utah.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/*class which converted System time into correct format*/
public class TimeManager {

    // return string like - dd_mm_yy
    public static String getCurrentDate() {
        String currentDate;
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy");
        currentDate = simpleDateFormat.format(date);
        return currentDate;
    }
}
