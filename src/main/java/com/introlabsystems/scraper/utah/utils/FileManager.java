package com.introlabsystems.scraper.utah.utils;

import com.introlabsystems.scraper.utah.converter.JsonConverter;
import com.introlabsystems.scraper.utah.model.Metadata;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.introlabsystems.scraper.utah.parser.MetadataStringParser.correctFileName;

@Slf4j
public class FileManager {
    private static final String FILE_FORMAT_PDF = ".pdf";
    private static final String FILE_FORMAT_JSON = ".json";
    private static final String DATA_DIRECTORY_NAME = "data";

    /*method gets directory`s path if it exist else create this directory */
    public static Path getDirectory(String nameState, String dateFolderName) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append(System.getProperty("user.dir"))
                .append(File.separator)
                .append(DATA_DIRECTORY_NAME)
                .append(File.separator)
                .append(nameState)
                .append(File.separator)
                .append(dateFolderName);
        Path directoryPath = Paths.get(stringBuilder.toString());
        /*check our dir. if not exist then create it*/
        if (!directoryPath.toFile().exists()) {
            try {
                Files.createDirectories(directoryPath);
                Thread.sleep(20000); // waiting 20 seconds while directories creating
                log.info("DIRECTORY CREATED!");
            } catch (IOException e) {
                log.error("IOException with creating directory {}", e.toString());
            } catch (InterruptedException e) {
                log.error("InterruptedException {}", e.getMessage());
            }
        }
        return directoryPath;
    }

    private static void saveDownloadingPdfFile(Metadata metadata, Path dirPath) {
        String correctTitleFile = correctFileName(metadata);
        StringBuilder sbFilePath = new StringBuilder();
        sbFilePath
                .append(dirPath)
                .append(File.separator)
                .append(correctTitleFile)
                .append(FILE_FORMAT_PDF);

        Path filePath = Paths.get(sbFilePath.toString());
        try {
            URL downloadedFileUrl = new URL(metadata.getLinkDownloadFile()); // create URL
            log.info("Downloading file " + correctTitleFile);
            copyFileFromURL(downloadedFileUrl, filePath);
            log.info("FILE WAS DOWNLOADED: {}", correctTitleFile);
        } catch (MalformedURLException e) {
            log.error("Incorrect URL address. Title: " + metadata.getTitleDownloadFile());
            log.error("MalformedURLException: " + e.toString());
        }
    }

    public static void saveAllPdfFileFromCollection(List<Metadata> list, Path path) {
        list.forEach(metadata -> saveDownloadingPdfFile(metadata, path));
    }

    public static void saveMetadataListToJsonFile(List<Metadata> metadalist, Path parentDirPath) {
        AtomicInteger metadataListIndex = new AtomicInteger();
        metadalist.forEach(metadata -> {
            try {
                Path jsonFilePAth = new FileManager().createJsonFilePathForMetadata(parentDirPath, metadata);
                String metadatasJsonString = JsonConverter.convertMetadataToJson(metadata);
                FileUtils.write(jsonFilePAth.toFile(), metadatasJsonString, Charset.forName("UTF-8"));
                metadataListIndex.getAndIncrement();
                log.info("metadata json string --> {}", metadatasJsonString);
                log.info("Index of saving Metadata list item --> {}", metadataListIndex);
            } catch (IOException e) {
                log.error("Cant create json file. Path: " + parentDirPath.toString());
                log.error("IOException:  " + e.getMessage());
            }
        });
        metadataListIndex.set(0);
    }

    public static void zipFolder(Path sourceFolderPath) throws IOException {
        try {
            FileOutputStream fos = new FileOutputStream(sourceFolderPath.toString() + ".zip");
            ZipOutputStream zos = new ZipOutputStream(fos);

            Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    ZipEntry zipEntry = new ZipEntry(sourceFolderPath.relativize(file).toFile().getName());
                    zos.putNextEntry(zipEntry);
                    FileInputStream fis = new FileInputStream(file.toFile());
                    zos.write(IOUtils.toByteArray(fis));
                    return FileVisitResult.CONTINUE;
                }
            });
            zos.flush();
            zos.close();
//            FileUtils.deleteDirectory(sourceFolderPath.toFile()); // deleting directory by apache
            deleteDirectory(sourceFolderPath.toFile());
        } catch (FileNotFoundException e) {
            log.info("Zipping folder is stopped");
            log.error("FileNotFoundException: " + e.toString());
        }
    }

    private static void deleteDirectory(File deleteFolder) {
        String[] entries = deleteFolder.list();
        try {
            for (String file : entries) {
                File currentFile = new File(deleteFolder, file);
                Files.delete(currentFile.toPath());
            }
            Files.delete(deleteFolder.toPath());
        } catch (IOException e) {
            log.error("Cant delete folder {}", e.getMessage());
        }
    }

    private static void copyFileFromURL(URL url, Path sourcePath) {
        try (InputStream inputStream = url.openStream()) {
            Files.copy(inputStream, sourcePath);
        } catch (IOException e) {
            log.error("copyFileFromURL IOException {}", e.toString());
        }
    }

    private Path createJsonFilePathForMetadata(Path parentDirPath, Metadata metadata) {
        String correctFileName = correctFileName(metadata);
        StringBuilder sbFilePath = new StringBuilder();
        sbFilePath
                .append(parentDirPath)
                .append(File.separator)
                .append(correctFileName)
                .append(FILE_FORMAT_JSON);
        return Paths.get(sbFilePath.toString());
    }
}
