package com.introlabsystems.scraper.utah;

import com.introlabsystems.scraper.utah.driver.WebDriverWrapper;
import com.introlabsystems.scraper.utah.model.Metadata;
import com.introlabsystems.scraper.utah.service.MetadataPageCollector;
import com.introlabsystems.scraper.utah.utils.FileManager;
import com.introlabsystems.scraper.utah.utils.TimeManager;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;
import java.util.List;

import static com.introlabsystems.scraper.utah.utils.Constant.BASE_URL;
import static com.introlabsystems.scraper.utah.utils.Constant.STATE_NAME;
import static com.introlabsystems.scraper.utah.utils.FileManager.*;

@Slf4j
public class Main {

    public static void main(String[] args) {

        WebDriverWrapper webDriverWrapper =
                new WebDriverWrapper(BASE_URL);

        MetadataPageCollector pageCollector = new MetadataPageCollector(webDriverWrapper);
        pageCollector.runScrapingData(1);
        String currentDate = TimeManager.getCurrentDate();
        List<Metadata> metadataCollection = pageCollector.getMetadataTotalList();
        Path directoryPath = FileManager.getDirectory(STATE_NAME, currentDate);

        // save List of metadata to json file
        saveMetadataListToJsonFile(metadataCollection, directoryPath);
        saveAllPdfFileFromCollection(metadataCollection, directoryPath);
        try {
            zipFolder(directoryPath);
        } catch (Exception e) {
            log.error("Cant zip folder. Path: %s", directoryPath.toString());
            log.error("Exception: %s", e.toString());
        }
    }

}
