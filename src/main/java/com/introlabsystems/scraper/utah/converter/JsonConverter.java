package com.introlabsystems.scraper.utah.converter;

import com.introlabsystems.scraper.utah.model.Metadata;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class JsonConverter {

    private static final ObjectMapper mapper = new ObjectMapper();

    public static String convertMetadataToJson(Metadata metadata) throws IOException {
        return mapper.writeValueAsString(metadata);
    }
}
