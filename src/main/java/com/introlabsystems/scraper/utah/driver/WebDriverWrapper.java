package com.introlabsystems.scraper.utah.driver;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WebDriverWrapper {

    private static final String PROPERTY_CHROME_KEY = "webdriver.chrome.driver";
    private static final String PROPERTY_CHROME_VALUE = System.getProperty("user.dir")
            + "/src/main/resources/chromedriver";
    private static final String COOKIES_POP_UP_BUTTON_CLASS_FOR_SELENIUM_DRIVER = "cc-compliance";
    private static final String COOKIES_POP_UP_BUTTON_CLASS_FOR_JAVASRIPT = ".cc-dismiss";
    private static final String NEXT_BUTTON_CLASS = ".next";

    @Getter
    private String baseUrl;
    private WebDriver webDriver;

    public WebDriverWrapper(String baseUrl) {
        this.baseUrl = baseUrl;
        initWebDriver();
    }

    private void initWebDriver() {
        checkProperty();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--no`-sandbox");

        webDriver = new ChromeDriver(options);
    }

    private void checkProperty() {
        if (System.getProperty(PROPERTY_CHROME_KEY) == null)
            System.setProperty(PROPERTY_CHROME_KEY, PROPERTY_CHROME_VALUE);
    }

    public void startInBrowser() {
        if (webDriver == null) {
            initWebDriver();
        }
        webDriver.get(baseUrl);
        closeCookiesPopUpByJavascript();
    }

    public String getPageSource() {
        return webDriver.getPageSource();
    }

    public void destroy() {
        webDriver.close();
    }

    public boolean exists(By by) {
        return webDriver.findElements(by).isEmpty();
    }

    private void closeCookiesPopUpBySelenium() {
        webDriver.findElement(By.className(COOKIES_POP_UP_BUTTON_CLASS_FOR_SELENIUM_DRIVER)).click();
    }

    private void closeCookiesPopUpByJavascript() {
        clickSelectorByJavascript(COOKIES_POP_UP_BUTTON_CLASS_FOR_JAVASRIPT);
    }

    public void onClickNextPage() {
        clickSelectorByJavascript(NEXT_BUTTON_CLASS);
    }

    private void clickSelectorByJavascript(String param) {
        if (webDriver instanceof JavascriptExecutor) {

            String javascriptRequest = "document.querySelector('" +
                    param +
                    "').click()";

            ((JavascriptExecutor) webDriver).executeScript(javascriptRequest);
        } else {
            throw new IllegalStateException("This driver does not support JavaScript!");
        }
    }

}
