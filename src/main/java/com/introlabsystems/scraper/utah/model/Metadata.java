package com.introlabsystems.scraper.utah.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Metadata {

    private String caseName;
    private String description;
    private String jurisdiction;
    private String caseNumber;
    private String briefField;

    private String link;
    private String linkDownloadFile;
    private String titleDownloadFile;
}
