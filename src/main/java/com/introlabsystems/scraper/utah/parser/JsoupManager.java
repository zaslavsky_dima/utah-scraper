package com.introlabsystems.scraper.utah.parser;

import com.introlabsystems.scraper.utah.model.Metadata;
import lombok.extern.java.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log
public class JsoupManager {

    private static final String CASE_BRIEF_DIRTY_SELECTOR = "#series-home > p.article-listing";
    private static final String LINK_SELECTOR = "a";
    private static final String LINK_ATTR_SELECTOR = "href";
    private static final String CASE_NUMBER_SELECTOR = "#identifier > p";
    private static final String CASE_FIELD_SELECTOR = "#publication_date > p";
    private static final String LINK_DOWNLOAD_FILE_SELECTOR = "a#pdf.btn";

    public List<Metadata> fetchDataFromBriefCollection(String sourceHtml) {
        List<Metadata> metadataList;

        Document jsoupDocument = Jsoup.parse(sourceHtml);
        Elements paragraphs = jsoupDocument.select(CASE_BRIEF_DIRTY_SELECTOR);

        metadataList = collectMetaDataFromElements(paragraphs);

        return metadataList;
    }

    private List<Metadata> collectMetaDataFromElements(Elements elements) {
        List<Metadata> collectionMetadata = new ArrayList<>();

        elements.forEach(element -> {
            MetadataStringParser metaDataStringParser = new MetadataStringParser(element.text());
            /* extracted fields from string*/
            String caseName = metaDataStringParser.fetchCaseName();
            String description = metaDataStringParser.fetchDescription();
            String jurisdiction = metaDataStringParser.fetchJurisdiction();
            String titleDownloadFile = metaDataStringParser.fetchTitleDownloadFile();
            /*selected field by selector*/
            String caseBriefLink = element.select(LINK_SELECTOR).attr(LINK_ATTR_SELECTOR);
            Document caseBriefDocument = createCaseBriefDocumentFromLink(caseBriefLink);
            /* Go to current CaseBrief and extract remaining data*/
            String caseNumber = fetchDataFromCaseBriefBySelector(
                    caseBriefDocument,
                    CASE_NUMBER_SELECTOR);
            String briefField = fetchDataFromCaseBriefBySelector(
                    caseBriefDocument,
                    CASE_FIELD_SELECTOR);
            String linkDownloadedFile = fetchDataFromCaseBriefBySelector(
                    caseBriefDocument,
                    LINK_DOWNLOAD_FILE_SELECTOR);
            Metadata metaData = buildMetaData(
                    caseName,
                    description,
                    jurisdiction,
                    caseBriefLink,
                    caseNumber,
                    briefField,
                    titleDownloadFile,
                    linkDownloadedFile);
            collectionMetadata.add(metaData);
            log.info("collectionMetadata size: " + collectionMetadata.size());
        });
        return collectionMetadata;
    }

    private String fetchDataFromCaseBriefBySelector(Document caseBriefDocument, String elementSelector) {
        try {
            Element element = caseBriefDocument.select(elementSelector).first();
            if (elementSelector.equals(LINK_DOWNLOAD_FILE_SELECTOR)) {
                return element.attr(LINK_ATTR_SELECTOR);
            }
            return element.text();
        } catch (NullPointerException e) {
            return null;
        }
    }

    private Metadata buildMetaData(
            String caseName,
            String description,
            String jurisdiction,
            String caseBriefLink,
            String caseNumber,
            String briefField,
            String nameDownloadFile,
            String linkDownloadFile) {

        Metadata metaData = new Metadata();
        metaData.setCaseName(caseName);
        metaData.setDescription(description);
        metaData.setJurisdiction(jurisdiction);
        metaData.setLink(caseBriefLink);
        metaData.setCaseNumber(caseNumber);
        metaData.setBriefField(briefField);
        metaData.setTitleDownloadFile(nameDownloadFile);
        metaData.setLinkDownloadFile(linkDownloadFile);

        return metaData;
    }

    private Document createCaseBriefDocumentFromLink(String caseBriefLink) {
        Document caseBriefDocument = null;
        try {
            caseBriefDocument = Jsoup.connect(caseBriefLink)
                    .timeout(3000)
                    .get();
        } catch (IOException e) {
            log.info("Cant create Document from link!"); // 94 or 95 cant create from link
        }

        return caseBriefDocument;
    }
}
