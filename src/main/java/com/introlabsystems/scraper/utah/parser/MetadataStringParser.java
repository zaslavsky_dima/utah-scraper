package com.introlabsystems.scraper.utah.parser;

import com.introlabsystems.scraper.utah.model.Metadata;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor
public class MetadataStringParser {

    private static final String TWO_DOTS_SEPARATOR = ":";
    private static final String COMMA_SEPARATOR = ",";
    private static final int SUBSTRING_INDEX_START = 0;
    private static final int SUBSTRING_FOR_LOW_INDEX_END = 185;
    private static final int SUBSTRING_FOR_HASH_INDEX_END = 190;

    private String metaDataString;

    public MetadataStringParser(String metaDataString) {
        this.metaDataString = metaDataString;
    }

    public static String replaceSearchedCharToChar(
            String str,
            String searchedChar,
            String replaceToChar) {
        return StringUtils.replaceChars(str, searchedChar, replaceToChar);
    }

    /*method which delete and replace slash "/" and  " " to "_" */
    public static String correctFileName(Metadata metadata) {
        String STRING_FORMAT = "%s_%s";

        String fileName = String.format(STRING_FORMAT, metadata.getCaseNumber(), metadata.getTitleDownloadFile());

        String fileNameWithOutSlash = replaceSearchedCharToChar(fileName, "/", "_");
        String fileNameWithOutSpace = replaceSearchedCharToChar(fileNameWithOutSlash, " ", "_");
        String lowCharacterWithHashCode = fileNameWithOutSpace.toLowerCase();
        // cut string to 185 chars
        String shortFileName = StringUtils.substring(
                lowCharacterWithHashCode,
                SUBSTRING_INDEX_START,
                SUBSTRING_FOR_LOW_INDEX_END);
        // concat string with hashcode original string
        String correctFileNameWithHashCode = String.format(
                STRING_FORMAT,
                shortFileName,
                metadata.getTitleDownloadFile().hashCode());

        return StringUtils.substring(correctFileNameWithHashCode, SUBSTRING_INDEX_START, SUBSTRING_FOR_HASH_INDEX_END);
    }

    public String fetchCaseName() {
        return StringUtils
                .substringBefore(metaDataString, TWO_DOTS_SEPARATOR);
    }

    public String fetchDescription() {
        String metaDataDescriptionDirty = StringUtils
                .substringAfter(metaDataString, TWO_DOTS_SEPARATOR);

        return StringUtils
                .substringBefore(metaDataDescriptionDirty, COMMA_SEPARATOR);
    }

    public String fetchJurisdiction() {
        return StringUtils
                .substringAfter(metaDataString, COMMA_SEPARATOR);
    }

    public String fetchTitleDownloadFile() {
        return StringUtils
                .substringBefore(metaDataString, COMMA_SEPARATOR);
    }
}
