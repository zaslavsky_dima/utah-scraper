package com.introlabsystems.scraper.utah.service;


import com.introlabsystems.scraper.utah.driver.WebDriverWrapper;
import com.introlabsystems.scraper.utah.model.Metadata;
import com.introlabsystems.scraper.utah.parser.JsoupManager;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/*This class collects meta data from <n> pages*/
public class MetadataPageCollector {

    private static final Logger logger = Logger.getLogger(MetadataPageCollector.class.getName());
    private WebDriverWrapper webDriverWrapper;
    @Getter
    private List<Metadata> metadataTotalList = new ArrayList<>();

    public MetadataPageCollector(WebDriverWrapper webDriverWrapper) {
        this.webDriverWrapper = webDriverWrapper;
    }

    public void runScrapingData(int numberPages) {
        webDriverWrapper.startInBrowser(); // open browser

        for (int i = 0; i < numberPages; i++) {
            JsoupManager jsoupManager = new JsoupManager();

            metadataTotalList.addAll(jsoupManager.fetchDataFromBriefCollection(webDriverWrapper.getPageSource()));

            webDriverWrapper.onClickNextPage();

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                logger.warning("Interrupted! " + e.getMessage());
            }
        }
        webDriverWrapper.destroy();
    }

}

